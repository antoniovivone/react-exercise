import { useState, useEffect } from 'react';
import './App.css';

const defaultValues = ["A", "B", "C", "D", "E"];

function App() {
  const [searchTerm, setSearchTerm] = useState("");
  const [searchResults, setSearchResults] = useState([]);
  const [itemsList, setItemsList] = useState([...defaultValues])

  //Fetching new data
  useEffect(() => {
    function searchMusic() {
      fetch(`https://itunes.apple.com/search?term=${searchTerm}&entity=album`)
        .then(response => response.json())
        .then(data => {
          const collectionNames = data.results.map(result => result.collectionName);

          function onlyUnique(value, index, self) {
            return self.indexOf(value) === index;
          }

          const sortedUniques = collectionNames.filter(onlyUnique).sort();
          const firstFives = sortedUniques.slice(0, 5)

          setSearchResults(firstFives);
        });
    }

    const timeOutId = setTimeout(() => {
      if (searchTerm !== "") {
        searchMusic();
      } else {
        setSearchResults([...defaultValues]);
      }
    }, 500);

    return () => clearTimeout(timeOutId);
  }, [searchTerm]);

  //Updating list
  useEffect(() => {
    function updateList() {
      let itemsListCopy = [...itemsList];
      let searchResultsCopy = [...searchResults];

      let element = itemsListCopy.shift();

      if (searchResults.length > 0) {
        element = searchResultsCopy.shift();
        searchResultsCopy.push(element);
        setSearchResults(searchResultsCopy)
      }

      itemsListCopy.push(element);

      setItemsList([...itemsListCopy]);
    }

    const intervalId = setInterval(() => {
      updateList();
    }, 1000);

    return () => clearInterval(intervalId)
  }, [itemsList, searchResults])

  return (
    <div className="App">
      <div className="container">
        <input type="text" name="searchTerm" placeholder="Search" value={searchTerm} onChange={event => setSearchTerm(event.target.value)} />
        <ul>
          {
            itemsList.map((item, index) => (
              <li key={index}>{item}</li>
            ))
          }
        </ul>
      </div>
    </div>
  );
}

export default App;